import { Certificate, Jobs, ToolsIcon } from "../types";

export const skillTools: Array<ToolsIcon> = [
    {
        id: 1,
        icon: require('../assets/img/icon/js.png'),
        text: 'Javascript'
    },
    {
        id: 2,
        icon: require('../assets/img/icon/ts.png'),
        text: 'Typescript'
    },
    {
        id: 3,
        icon: require('../assets/img/icon/python.png'),
        text: 'Python'
    },
    {
        id: 4,
        icon: require('../assets/img/icon/github.png'),
        text: 'Github'
    },
    {
        id: 5,
        icon: require('../assets/img/icon/gitlab.png'),
        text: 'Gitlab'
    },
    {
        id: 6,
        icon: require('../assets/img/icon/git.png'),
        text: 'GIT'
    },
    {
        id: 7,
        icon: require('../assets/img/icon/jira.png'),
        text: 'Jira'
    },
    {
        id: 8,
        icon: require('../assets/img/icon/bitbucket.png'),
        text: 'Bitbucket'
    },
    {
        id: 9,
        icon: require('../assets/img/icon/npm.png'),
        text: 'Npm'
    },
    {
        id: 10,
        icon: require('../assets/img/icon/yarn.png'),
        text: 'Yarn'
    },
    {
        id: 11,
        icon: require('../assets/img/icon/node.png'),
        text: 'Node js'
    },
    {
        id: 12,
        icon: require('../assets/img/icon/react.png'),
        text: 'React'
    },
    {
        id: 13,
        icon: require('../assets/img/icon/next.png'),
        text: 'Next Js'
    },
    {
        id: 14,
        icon: require('../assets/img/icon/vercel.png'),
        text: 'Vercel'
    },
    {
        id: 15,
        icon: require('../assets/img/icon/docker.png'),
        text: 'Docker'
    },
    {
        id: 16,
        icon: require('../assets/img/icon/gcp.webp'),
        text: 'GCP'
    },
    {
        id: 17,
        icon: require('../assets/img/icon/tensorflow.png'),
        text: 'Tensorflow'
    },
    {
        id: 18,
        icon: require('../assets/img/icon/pytorch.png'),
        text: 'Pytorch'
    },
    {
        id: 19,
        icon: require('../assets/img/icon/aws.png'),
        text: 'AWS'
    },
    {
        id: 20,
        icon: require('../assets/img/icon/fastapi.png'),
        text: 'FastApi'
    },
    {
        id: 21,
        icon: require('../assets/img/icon/postgre.png'),
        text: 'Postgre'
    },
    {
        id: 22,
        icon: require('../assets/img/icon/tailwind.png'),
        text: 'Tailwind'
    },
    {
        id: 23,
        icon: require('../assets/img/icon/mui.png'),
        text: 'MUI'
    },
    {
        id: 24,
        icon: require('../assets/img/icon/redux.png'),
        text: 'Redux'
    }
]


export const myJob: Array<Jobs> = [
    {
        id: 1,
        name: 'Middleware Admin Panel',
        href: 'Middleware',
        category: 'company'
    },
    {
        id: 2,
        name: 'TLaunchpad Admin Panel',
        href: 'TLaunchpad',
        category: 'company'
    },
    {
        id: 3,
        name: 'Kriptoversity Admin Panel',
        href: 'Kriptoversity',
        category: 'company'
    },
    {
        id: 4,
        name: 'Kioser Admin Panel',
        href: 'Kioser',
        category: 'company'
    },
    {
        id: 5,
        name: 'Kickin App',
        href: 'Kickin',
        category: 'company'
    },
    {
        id: 6,
        name: 'React Redux',
        href: 'https://github.com/feryrmdhn/react-redux',
        category: 'personal'
    },
    {
        id: 7,
        name: 'Dummy Eduqat Web Builder',
        href: 'https://eduqat-puck.vercel.app/',
        category: 'personal'
    },
    {
        id: 8,
        name: 'Metamask',
        href: 'https://github.com/feryrmdhn/connect-metamask',
        category: 'personal'
    },
    {
        id: 9,
        name: 'Next Context',
        href: 'https://github.com/feryrmdhn/nextjs-context',
        category: 'personal'
    },
    {
        id: 10,
        name: 'Books Classification & Recommender System',
        href: 'https://github.com/feryrmdhn/book-recommender-system',
        category: 'personal'
    },
    {
        id: 11,
        name: 'React with Drag n Drop',
        href: 'https://github.com/feryrmdhn/lms-simple-crud',
        category: 'personal'
    },
    {
        id: 12,
        name: 'Eduqat Dashboard',
        href: 'Eduqat Dashboard',
        category: 'company'
    }
]

export const myCerfitiface: Array<Certificate> = [
    {
        id: 1,
        title: 'English Level Test',
        award: 'EF Set',
        href: 'https://www.efset.org/cert/LVXeAD',
        date: 'Jul 2020'
    },
    {
        id: 2,
        title: 'Machine Learning Foundations',
        award: 'AWS Academy',
        href: 'https://drive.google.com/file/d/1utbJsJ1FhEaPk8TZuUEJTKosAeC0vA2S/view?usp=sharing',
        date: 'Mar 2024'
    },
    {
        id: 3,
        title: 'Machine Learning Specialization',
        award: 'Coursera',
        href: 'https://drive.google.com/file/d/1iDoFrl3khQi02msZYAVbtF-kbeYNNZRM/view?usp=sharing',
        date: 'Mar 2024'
    },
    {
        id: 4,
        title: 'Problem Solving (Intermediate Level)',
        award: 'Hackerrank',
        href: 'https://www.hackerrank.com/certificates/2d7876c0a13b',
        date: 'Sep 2021'
    },
    {
        id: 5,
        title: 'React (Basic Level)',
        award: 'Hackerrank',
        href: 'https://www.hackerrank.com/certificates/f606a3ed992c',
        date: 'Oct 2021'
    }
]
import { FC, useEffect, useState } from "react";
import { Box, Container, Flex, Heading, List, Link, ListItem, Text, useColorModeValue, Button, Center, Highlight } from "@chakra-ui/react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFaceGrinWide, faLocationDot, faMedal, faPersonRunning, faUser } from "@fortawesome/free-solid-svg-icons";
import { works } from "../utils";

const About: FC = () => {
    const [limit, setLimit] = useState<number>(5)
    const colorText = useColorModeValue('black', 'white')

    useEffect(() => {
        document.title = 'Fery Ramadhan | About';
    }, [])

    const Titlejob = ({ children }: any) => {
        return <Heading
            as='h5'
            fontSize={20}
            fontFamily={'sans-serif'}
            fontWeight={'normal'}
            color={useColorModeValue('gray.600', '#d1d100')}
        >
            {children}
        </Heading>
    }

    let limitData: number = works.length

    return (
        <Box h='full' pt={5}>
            <Container maxW='container.lg'>
                <Heading
                    as='h2'
                    mb={4}
                    fontFamily='sans-serif'
                    color={useColorModeValue('gray.600', '#d1d100')}
                >
                    About me
                </Heading>
                <div className="fadeInUp">
                    <Text
                        fontSize={18}
                        fontFamily='monospace'
                        color={colorText}
                        mb={5}
                    >
                        <Highlight query='Fery Ramadhan Cahyono' styles={{ fontWeight: 'bold', color: useColorModeValue('gray.600', '#d1d100') }}>
                            Hello guys, here is Fery Ramadhan Cahyono currently working as frontend engineer, and are interested in exploring AI or ML
                        </Highlight>
                    </Text>
                    <Text fontSize={14} color={colorText} mb={5}>
                        Developing web-based projects is very fun, especially when combined with the ability to apply Artificial Intelligence concepts,
                        it is very challenging, that's what I'm after. It would be great if we could help people develop their business.
                    </Text>
                    <Heading
                        as='h3'
                        mb={5}
                        fontSize={20}
                        fontFamily='sans-serif'
                        fontWeight='normal'
                        color={useColorModeValue('gray.600', '#d1d100')}
                    >
                        Summary :
                    </Heading>
                    <Box mb={5}>
                        <List px={10}>
                            <ListItem mb={2}>
                                <Flex flex={{ base: 1 }} justify={{ md: 'start' }}>
                                    <FontAwesomeIcon icon={faUser} width={15} />
                                    <Text ml={10}>The first child</Text>
                                </Flex>
                            </ListItem>
                            <ListItem mb={2}>
                                <Flex flex={{ base: 1 }} justify={{ md: 'start' }}>
                                    <FontAwesomeIcon icon={faLocationDot} width={15} />
                                    <Text ml={10}>Indonesia</Text>
                                </Flex>
                            </ListItem>
                            <ListItem mb={2}>
                                <Flex flex={{ base: 1 }} justify={{ md: 'start' }}>
                                    <FontAwesomeIcon icon={faMedal} width={15} />
                                    <Text ml={10}>Bachelor</Text>
                                </Flex>
                            </ListItem>
                            <ListItem mb={2}>
                                <Flex flex={{ base: 1 }} justify={{ md: 'start' }}>
                                    <FontAwesomeIcon icon={faFaceGrinWide} width={15} />
                                    <Text ml={10}>Playful</Text>
                                </Flex>
                            </ListItem>
                            <ListItem mb={2}>
                                <Flex flex={{ base: 1 }} justify={{ md: 'start' }}>
                                    <FontAwesomeIcon icon={faPersonRunning} width={15} />
                                    <Text ml={10}>Love Sports</Text>
                                </Flex>
                            </ListItem>
                        </List>
                    </Box>
                    <Heading
                        as='h3'
                        mb={5}
                        fontSize={20}
                        fontFamily='sans-serif'
                        fontWeight='normal'
                        color={useColorModeValue('gray.600', '#d1d100')}
                    >
                        Experience :
                    </Heading>
                    <Box mb={5}>
                        {works.slice(0, limit).map((data, i) => (
                            <List key={i} px={10}>
                                <Flex
                                    flex={{ base: 1 }}
                                    justify={{ md: 'start' }}
                                    align={'normal'}
                                    mb={4}
                                >
                                    {data.icon}
                                    <List spacing={1} ml={5}>
                                        <Titlejob>{data.title}</Titlejob>
                                        <ListItem>
                                            <Link href={data.href}>
                                                <b>{data.company}</b>
                                            </Link>
                                        </ListItem>
                                        <ListItem>{data.status}</ListItem>
                                        <ListItem>{data.time}</ListItem>
                                        <ListItem>{data.location}</ListItem>
                                    </List>
                                </Flex>
                            </List>
                        ))}
                    </Box>
                    <Center>
                        {works.slice(0, limit).length !== limitData &&
                            <Button variant='outline' size='sm' onClick={() => setLimit(limit + 2)}>Load More...</Button>
                        }
                    </Center>
                </div>
            </Container>
        </Box>
    )
}

export default About;
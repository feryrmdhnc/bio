import { FC, useEffect, useState } from "react";
import { Box, Button, Center, Container, Heading, Link, List, ListItem, Text, useColorModeValue } from "@chakra-ui/react";
import { article } from "../utils";

const Blog: FC = () => {
    const [limit, setLimit] = useState<number>(5)
    const colorText = useColorModeValue('black', 'white')
    const borderColor = useColorModeValue('#0a0a0a2b', '#ffffff29')

    useEffect(() => {
        document.title = 'Fery Ramadhan | Blog';
    }, [])

    return (
        <>
            <Box h='full' pt={5}>
                <Container maxW='container.lg' px={6} color={useColorModeValue('gray.600', '#d1d100')}>
                    <Heading
                        as='h2'
                        mb={4}
                        fontFamily='sans-serif'
                        color={useColorModeValue('gray.600', '#d1d100')}
                    >
                        Blogs
                    </Heading>
                    <div className="fadeInUp">
                        <Text
                            fontSize={18}
                            fontFamily='monospace'
                            color={colorText}
                            mb={7}
                        >
                            I also wrote some articles that might help some people, especially in the field of web development & ai/ml
                        </Text>
                        <List mb={10}>
                            {article.slice(0, limit).map((list) => (
                                <ListItem key={list.id} mb={6} pb={3} borderBottom={`1px solid ${borderColor}`}>
                                    <Link
                                        href={list.href}
                                        fontSize={20}
                                    >
                                        {list.title}
                                    </Link>
                                    <Text
                                        color={colorText}
                                        fontSize={14}
                                        pt={3}
                                        fontStyle='italic'
                                    >
                                        {list.date}
                                    </Text>
                                </ListItem>
                            ))}
                        </List>
                        <Center>
                            {article.length > 5 &&
                                <Button variant='outline' size='sm' onClick={() => setLimit(limit + 2)}>Load More...</Button>
                            }
                        </Center>
                    </div>
                </Container>
            </Box>
        </>
    )
}

export default Blog;
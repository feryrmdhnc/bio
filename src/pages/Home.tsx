import { FC, useEffect } from "react";
import { Box, Button, Container, Flex, Text, useColorModeValue } from "@chakra-ui/react";
import Typewriter from 'typewriter-effect';
import { ArrowForwardIcon } from "@chakra-ui/icons";
import { Link as Links } from 'react-router-dom';

const Home: FC = () => {
    const colorText = useColorModeValue('black', 'white')

    useEffect(() => {
        document.title = 'Fery Ramadhan';
    }, [])

    return (
        <>
            <Container maxW='container.sm' minH='100%' display='flex' justifyContent='center' alignItems='center' color={useColorModeValue('gray.600', '#d1d100')}>
                <Box mt={170}>
                    <Typewriter
                        options={{
                            strings: ['Welcome.. ', 'Selamat Datang.. '],
                            autoStart: true,
                            loop: true,
                            wrapperClassName: 'typewriter-text',
                            // cursorClassName: 'typewriter-cursor'
                        }}
                    />
                    <div className="fadeInUp">
                        <Text
                            fontSize={18}
                            fontFamily='monospace'
                            color={colorText}
                        >
                            Hey there, I'm <b>Fery</b> an engineer
                            who develop web interface and also interested working on ML projects.
                        </Text>
                        <Flex justifyContent='right'>
                            <Links to='/about'>
                                <Button
                                    w={100}
                                    mt={4}
                                    background='none'
                                    border='2px'
                                    borderStyle='solid'
                                    borderRadius={0}
                                    rightIcon={<ArrowForwardIcon />}
                                    className="btn-go"
                                >
                                    Go
                                </Button>
                            </Links>
                        </Flex>
                    </div>
                </Box>
            </Container>
        </>
    )
}

export default Home;
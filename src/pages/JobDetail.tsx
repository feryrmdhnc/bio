import { FC, useEffect } from "react";
import { Box, Container, Flex, Heading, Tab, TabList, TabPanel, TabPanels, Tabs, Text, useColorModeValue } from "@chakra-ui/react";
import { useParams } from "react-router-dom";
import Carousel from "../components/Carousel";
import { data } from "../utils";

const JobDetail: FC = () => {
    const { slug } = useParams<string>()
    const colorText = useColorModeValue('black', 'white')
    const borderColor = useColorModeValue('#0a0a0a2b', '#ffffff29')
    const colorTitle = useColorModeValue('gray.600', '#d1d100')

    useEffect(() => {
        document.title = 'Fery Ramadhan | Project Detail';
    }, [])

    return (
        <>
            <Box h='full' pt={5}>
                <Container maxW='container.lg' px={6} color={useColorModeValue('gray.600', '#d1d100')}>
                    <Heading
                        as='h2'
                        mb={4}
                        fontFamily='sans-serif'
                        color={useColorModeValue('gray.600', '#d1d100')}
                    >
                        {slug}
                    </Heading>
                    <div className="fadeInUp">
                        <Box border={`0.5px solid ${borderColor}`} borderRadius={'6px'}>
                            <Carousel data={data.filter(list => list.slug === slug)} />
                        </Box>
                        {data.filter(list => list.slug === slug).map((item, i) => (
                            <Box key={i} mt={8}>
                                <Heading
                                    as='h4'
                                    mb={4}
                                    fontFamily='sans-serif'
                                    fontSize='2xl'
                                    color={colorTitle}
                                >
                                    Description
                                </Heading>
                                <Text
                                    fontSize={17}
                                    fontFamily='sans-serif'
                                    color={colorText}
                                    mb={10}
                                >
                                    {item.desc}
                                </Text>
                                <Heading
                                    as='h4'
                                    mb={4}
                                    fontFamily='sans-serif'
                                    fontSize='2xl'
                                    color={colorTitle}
                                >
                                    Tech Stack
                                </Heading>
                                <Text
                                    fontSize={14}
                                    fontFamily='monospace'
                                    color={colorText}
                                    mb={6}
                                >
                                    The following is the technology stack used to build this project.
                                </Text>
                                <Box mb={'5'}>
                                    <Tabs variant='unstyled'>
                                        <Flex>
                                            <TabList flexDirection='column'>
                                                <Tab
                                                    pl={0}
                                                    _selected={{
                                                        fontWeight: 'bold',
                                                        _before: { opacity: 1 }
                                                    }}
                                                    _before={{
                                                        content: '"\u21B3"',
                                                        marginRight: '10px',
                                                        opacity: 0,
                                                        transition: 'opacity 0.2s'
                                                    }}
                                                >
                                                    Frontend
                                                </Tab>
                                                <Tab
                                                    pl={0}
                                                    _selected={{
                                                        fontWeight: 'bold',
                                                        _before: { opacity: 1 }
                                                    }}
                                                    _before={{
                                                        content: '"\u21B3"',
                                                        marginRight: '10px',
                                                        opacity: 0,
                                                        transition: 'opacity 0.2s'
                                                    }}
                                                >
                                                    Backend
                                                </Tab>
                                            </TabList>
                                            <TabPanels>
                                                <TabPanel>
                                                    {item.frontend?.map((fe, idx) => (
                                                        <Box key={idx} ml='6' fontSize={'lg'} color={colorText}>
                                                            &bull; {fe}
                                                        </Box>
                                                    ))}
                                                </TabPanel>
                                                <TabPanel>
                                                    {item.backend?.map((be, idx) => (
                                                        <Box key={idx} ml='6' fontSize={'lg'} color={colorText}>
                                                            &bull; {be}
                                                        </Box>
                                                    ))}
                                                </TabPanel>
                                            </TabPanels>
                                        </Flex>
                                    </Tabs>
                                </Box>
                                <Text
                                    fontSize={12}
                                    fontFamily='sans-serif'
                                    color={colorText}
                                    mb={10}
                                >
                                    Note: The images used is from the demo version of the project.
                                </Text>
                            </Box>
                        ))}
                    </div>
                </Container>
            </Box>
        </>
    )
}

export default JobDetail;
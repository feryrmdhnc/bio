import { Articles, Experience, JobDetails } from "../types"

export const works: Array<Experience> = [
    {
        icon: <img src={require('../assets/img/no-pict.jpg')} className='company-icon' alt='Eduqat' />,
        title: 'AI/ML Engineer',
        company: '-',
        status: 'Freelance',
        time: 'Aug 2024 - Present',
        location: 'Anywhere',
        href: '#'
    },
    {
        icon: <img src={require('../assets/img/eduqat.jpeg')} className='company-icon' alt='Eduqat' />,
        title: 'Sr. Frontend Engineer',
        company: 'Eduqat',
        status: 'Remote Fulltime',
        time: 'Jan 2023 - Present',
        location: 'South Jakarta, DKI Jakarta',
        href: 'https://eduqat.com/'
    },
    {
        icon: <img src={require('../assets/img/TC.png')} className='company-icon' alt='Tokocrytpo' />,
        title: 'Frontend Developer',
        company: 'Tokocrypto',
        status: 'Remote Fulltime',
        time: 'Jan 2022 - Dec 2022',
        location: 'Jakarta, DKI Jakarta',
        href: 'https://www.tokocrypto.com/'
    },
    {
        icon: <img src={require('../assets/img/RG.png')} className='company-icon' alt='Ruangguru' />,
        title: 'Frontend Mentor',
        company: 'Ruangguru',
        status: 'Remote Freelance',
        time: 'Feb 2022 - Mar 2022',
        location: 'South Jakarta, DKI Jakarta',
        href: 'https://www.ruangguru.com/'
    },
    {
        icon: <img src={require('../assets/img/Kio.png')} className='company-icon' alt='Kioser' />,
        title: 'Software Engineer',
        company: 'Kioser',
        status: 'Fulltime',
        time: 'Dec 2020 - Jan 2022',
        location: 'Makassar, South Sulawesi',
        href: 'https://kioser.com/'
    },
    {
        icon: <img src={require('../assets/img/serpul.webp')} className='company-icon' alt='Kioser' />,
        title: 'Software Engineer',
        company: 'Serpul',
        status: 'Contract',
        time: 'Oct 2020 - Dec 2020',
        location: 'Makassar, South Sulawesi',
        href: 'https://serpul.co.id/'
    },
    {
        icon: <img src={require('../assets/img/Gl.png')} className='company-icon' alt='Glints' />,
        title: 'Frontend Developer',
        company: 'Glints Indonesia',
        status: 'Internship',
        time: 'Apr 2020 - Jul 2020',
        location: 'Batam, Kepulauan Riau',
        href: 'https://glints.com/id'
    },
    {
        icon: <img src={require('../assets/img/Gsi.jpeg')} className='company-icon' alt='GSI' />,
        title: 'Cost Control',
        company: 'GSI',
        status: 'Contract',
        time: 'Aug 2019 - Jan 2020',
        location: 'Kolaka, Southeast Sulawesi',
        href: 'http://www.gsicorp.co.id/'
    },
    {
        icon: <img src={require('../assets/img/hor.png')} className='company-icon' alt='Horison' />,
        title: 'IT Officer',
        company: 'Hotel Horison',
        status: 'Contract',
        time: 'Dec 2018 - Apr 2019',
        location: 'Makassar, South Sulawesi',
        href: 'https://myhorison.com/hotelprofile/view?hotel=horison-ultima-makassar'
    }
]

export const article: Array<Articles> = [
    {
        id: 1,
        title: 'JumpStart Sagemaker Model for Image Classification',
        date: 'Jan 09, 2025',
        href: 'https://medium.com/@feryramadhanc_/jumpstart-sagemaker-model-for-image-classification-0c65de66b5af'
    },
    {
        id: 2,
        title: 'Analisis Data Dasar menggunakan Python',
        date: 'Dec 31, 2024',
        href: 'https://medium.com/@feryramadhanc_/analisis-data-dasar-menggunakan-python-2f19d3b071c1'
    },
    {
        id: 3,
        title: 'AutoML with Vertex AI',
        date: 'Dec 14, 2024',
        href: 'https://medium.com/@feryramadhanc_/automl-with-vertex-ai-8f55c4770d83'
    },
    {
        id: 4,
        title: 'Sagemaker Autopilot Really Interesting!',
        date: 'Dec 03, 2024',
        href: 'https://medium.com/@feryramadhanc_/sagemaker-autopilot-really-interesting-30a22bedc17b'
    },
    {
        id: 5,
        title: 'XGBoost with training data from Amazon RDS',
        date: 'Aug 18, 2024',
        href: 'https://medium.com/@feryramadhanc_/xgboost-with-training-data-from-amazon-rds-b7535089c9b5'
    },
    {
        id: 6,
        title: 'Integrating Amazon SageMaker with FastAPI for Local Machine Learning Predictions',
        date: 'Aug 16, 2024',
        href: 'https://medium.com/@feryramadhanc_/integrating-amazon-sagemaker-with-fastapi-for-local-machine-learning-predictions-f6c3b661f390'
    },
    {
        id: 7,
        title: 'Books Classification & Recommendation using Content Based Filtering',
        date: 'Mar 9, 2024',
        href: 'https://medium.com/@feryramadhanc_/books-classification-recommendation-using-content-based-filtering-6545daaf8a98'
    },
    {
        id: 8,
        title: 'Mengenal Kecerdasan Buatan - AI (Artificial Intelligence)',
        date: 'Jul 6, 2023',
        href: 'https://medium.com/@feryramadhanc_/mengenal-kecerdasan-buatan-ai-artificial-intelligence-fcd5414415eb'
    },
    {
        id: 9,
        title: 'Perbedaan Javascript dan Typescript',
        date: 'Nov 5, 2022',
        href: 'https://medium.com/@feryramadhanc_/perbedaan-javascript-dan-typescript-9586c9f2773b'
    },
    {
        id: 10,
        title: 'Easy! Cara menghubungkan Metamask wallet dengan React',
        date: 'May 29, 2022',
        href: 'https://medium.com/@feryramadhanc_/easy-cara-menghubungkan-metamask-wallet-dengan-react-6450aa8cd575'
    },
    {
        id: 11,
        title: 'Zustand React Tutorial — Pengganti Redux (?)',
        date: 'Feb 20, 2022',
        href: 'https://medium.com/@feryramadhanc_/zustand-react-tutorial-pengganti-redux-abc0e384bbbe'
    }
]

export const data: Array<JobDetails> = [
    {
        slug: 'Eduqat Dashboard',
        desc: 'Eduqat is a digital education platform based on artificial intelligence (AI). Through this dashboard that is used to manage schools, classes, courses, and their users. This also has an excellent feature from Eduqat, namely creating an AI-based school and builder landing page of school website.',
        frontend: ['React', 'Redux-Thunk', 'Styled-component', 'Tailwind', 'AWS', 'Ant Design'],
        backend: ['Nodejs', 'AWS', 'PostgreSQL', 'Postman', 'OpenAI', 'Dynamo DB'],
        image: [
            <img src={require('../assets/img/project/edu1.png')} alt="eduqat1" loading="lazy" />,
            <img src={require('../assets/img/project/edu2.png')} alt="eduqat2" loading="lazy" />,
            <img src={require('../assets/img/project/edu3.png')} alt="eduqat3" loading="lazy" />,
            <img src={require('../assets/img/project/edu4.png')} alt="eduqat4" loading="lazy" />,
        ]
    },
    {
        slug: 'Middleware',
        desc: 'Middleware payment aims to make it easier for customers to make transactions on several integrated company projects, both internal and external transaction facilities, and also this project is to minimize the cost of the company or in other words reduce the use of third party platform.',
        frontend: ['Typescript', 'React', 'Redux-Saga', 'Scss', 'React-Bootstrap', 'Vercel'],
        backend: ['Nodejs', 'Typescript', 'Nest', 'Swagger', 'MySQL'],
        image: [
            <img src={require('../assets/img/project/mid1.png')} alt="midlleware1" loading="lazy" />,
            <img src={require('../assets/img/project/mid2.png')} alt="midlleware2" loading="lazy" />,
            <img src={require('../assets/img/project/mid3.png')} alt="midlleware3" loading="lazy" />,
            <img src={require('../assets/img/project/mid4.png')} alt="midlleware4" loading="lazy" />,
        ]
    },
    {
        slug: 'Kioser',
        desc: 'Kioser is a distributor for selling online credit for all operators without large national capital, providing a variety of products ranging from All Operator Electric Credit, Internet Packages, SMS Packages, Transfer Credit, PLN Tokens, Game Vouchers, eCommerce Balances and Payment Point Online Banking (PPOB) at a low price!',
        frontend: ['Javascript', 'React', 'Redux-Thunk', 'Scss', 'Ant Design'],
        backend: ['PHP', 'Laravel', 'Golang', 'MySQL', 'Nginx'],
        image: [
            <img src={require('../assets/img/project/ki1.PNG')} alt="kioser1" loading="lazy" />,
            <img src={require('../assets/img/project/ki2.PNG')} alt="kioser2" loading="lazy" />,
            <img src={require('../assets/img/project/ki3.png')} alt="kioser3" loading="lazy" />,
            <img src={require('../assets/img/project/ki4.PNG')} alt="kioser4" loading="lazy" />,
        ]
    },
    {
        slug: 'TLaunchpad',
        desc: 'TLaunchpad is the leading decentralized fundraising platform and IDO platform in the Asian market. This project invites to be part of a cross-chain token pool and raise capital on Blockchains : Solana, Binance Smart Chain, Polygon, and Ethereum.',
        frontend: ['Typescript', 'Nextjs', 'Context', 'Scss', 'Material UI', 'Web3', 'Vercel'],
        backend: ['Nodejs', 'Typescript', 'Nest', 'Express', 'Swagger', 'Web3', 'MongoDB'],
        image: [
            <img src={require('../assets/img/project/launch-1.png')} alt="launch1" loading="lazy" />,
            <img src={require('../assets/img/project/launch-2.png')} alt="launch2" loading="lazy" />,
            <img src={require('../assets/img/project/launch-3.png')} alt="launch3" loading="lazy" />,
            <img src={require('../assets/img/project/launch-4.png')} alt="launch4" loading="lazy" />,
        ]
    },
    {
        slug: 'Kriptoversity',
        desc: 'Cryptoversity is a platform to educate people about crypto, blockchain, NFT, web3, exchange and so on. This platform is also available in mobile applications with Android and iOS Operating System.',
        frontend: ['Javascript', 'React', 'Redux-Saga', 'Scss', 'React-Bootstrap', 'Vercel'],
        backend: ['Nodejs', 'Typescript', 'Nest', 'Swagger', 'MySQL', 'AWS'],
        image: [
            <img src={require('../assets/img/project/Krip-1.png')} alt="krip1" loading="lazy" />,
            <img src={require('../assets/img/project/Krip-2.png')} alt="krip2" loading="lazy" />,
            <img src={require('../assets/img/project/Krip-3.png')} alt="krip3" loading="lazy" />,
            <img src={require('../assets/img/project/Krip-4.png')} alt="krip4" loading="lazy" />,
        ]
    },
    {
        slug: 'Kickin',
        desc: 'Kickin is an application for ordering long distance futsal courts online. It is possible for clients without having to come directly to the field. Please visit dummy web on kickinapp.herokuapp.com/ to see it.',
        frontend: ['Javascript', 'React', 'Redux-Thunk', 'Scss', 'Ant Design', 'Heroku'],
        backend: ['Nodejs', 'Javascript', 'Express', 'Postman Docs', 'PostgreSQL', 'Nginx'],
        image: [
            <img src={require('../assets/img/project/Kick1.png')} alt="Kick1" loading="lazy" />,
            <img src={require('../assets/img/project/Kick2.png')} alt="Kick2" loading="lazy" />,
            <img src={require('../assets/img/project/Kick3.png')} alt="Kick3" loading="lazy" />,
        ]
    }
]